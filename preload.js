// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
window.addEventListener('DOMContentLoaded', () => {
  const replaceText = (selector, text) => {
    const element = document.getElementById(selector);
    if (element) element.innerText = text;
  }

  for (const type of ['chrome', 'node', 'electron']) {
    replaceText(`${type}-version`, process.versions[type]);
  }
});

console.log("###preload process###");

window.ipcRenderer = require('electron').ipcRenderer;

// const { ipcRenderer } = require('electron');
// console.log(ipcRenderer.sendSync('synchronous-message', 'ping')); // prints "pong"
//
// ipcRenderer.on('asynchronous-reply', (event, arg) => {
//     console.log(arg); // prints "pong"
// });
// ipcRenderer.send('asynchronous-message', 'ping');
//
// console.log("###renderer process###");

ipcRenderer.on("ping", (event,args) => {
  console.log("---ping", args);
  let options = {};
  options.detail = args;
  const cusEvent = new CustomEvent('fetch-data', options);
  window.dispatchEvent(cusEvent);
});